import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import Changeset from 'ember-changeset';
import lookupValidator from 'ember-changeset-validations';
import MediaValidations from '../validations/media';

export default class ApplicationRoute extends Route {
  @service intl;
  @service store;

  beforeModel() {
    super.beforeModel(...arguments);
    return this.intl.setLocale(['cs']);
  }

  model() {
    let media = this.store.createRecord('media', {
      title: 'a title',
      url: 'http',
    });
    let validatorFn = lookupValidator(MediaValidations);
    let changeset = new Changeset(media, validatorFn, MediaValidations);
    changeset.validate();
    return changeset;
  }
}
