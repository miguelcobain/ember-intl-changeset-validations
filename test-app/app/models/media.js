import Model, { attr } from '@ember-data/model';

export default class MediaModel extends Model {
  @attr('string') title;
  @attr('string') url;
  @attr('string') insertedAt;
  @attr('string') updatedAt;
}
