# ember-intl-changeset-validations

Adds support for [ember-intl](https://github.com/ember-intl/ember-intl) to [ember-changeset-validations](https://github.com/poteto/ember-changeset-validations).

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg)](code-of-conduct.md)
[![Ember Observer Score](https://emberobserver.com/badges/ember-intl-changeset-validations.svg)](https://emberobserver.com/addons/ember-intl-changeset-validations)

## Installation

```sh
ember install ember-intl-changeset-validations
```

## Usage

In your project create a file with translations:

```json
// /translations/validations/cs.json
{
  "validations": {
    "inclusion": "Musí být vybráno",
    "present": "Prosím vyplňte",
    "between": "Musí mít mezi {min} a {max} znaky",
    "invalid": "Neplatný formát",
    "email": "Nplatná e-mailová adresa"
  }
}
```

- Example is for the `cs` locale.
- All validation messages have to be nested under `validations` key.
- Make sure to check the list of all the possible [default validation messages](https://github.com/poteto/ember-changeset-validations#overriding-validation-messages).

From now on all the validation messages from `ember-changeset-validation` should be translated.

You can see it in action in the dummy app:
 - [/translations/validations/cs.json](/translations/validations/cs.json)
 - [/tests/dummy/app/application/route.js](/tests/dummy/app/application/route.js)
 - [/tests/dummy/app/application/template.hbs](/tests/dummy/app/application/template.hbs)

![Example dummy app](/doc/screenshot.gif)

## Compatibility

* Ember.js v3.20 or above
* Ember CLI v3.20 or above
* Node.js v12 or above

## Credits

- Big thanks to [ember-i18n-changeset-validations](https://github.com/mirai-audio/ember-i18n-changeset-validations) that showed me 90% of the how-to needed to make this work.

## License

This project is licensed under the [MIT License](LICENSE.md).
